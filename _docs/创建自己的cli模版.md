## 创建自己的cli模版

- https://zhuanlan.zhihu.com/p/362762954
- https://cli.vuejs.org/zh/guide/plugins-and-presets.html
- https://www.jianshu.com/p/44a37ed1071e
- https://segmentfault.com/a/1190000016389996
- https://gitee.com/LuDun123/vue-cli-preset/tree/master/
- https://note.xiexuefeng.cc/post/vue-cli-remote-preset/

runtimeCompiler: true,


## 扩展模版

**参考文档（必看）**

- 官网地址  
  https://cli.vuejs.org/zh/guide/plugins-and-presets.html#preset  

- cli插件开发指南  
  https://cli.vuejs.org/zh/dev-guide/plugin-dev.html#%E5%BC%80%E5%A7%8B

**参考项目**
- https://gitee.com/LuDun123/dashboard/projects


 