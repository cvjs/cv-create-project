import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import './samples/node-api';

// 引入路由
import router from './router';

const app = createApp(App);

app
  .use(router)
  .mount('#app')
  .$nextTick(() => {
    postMessage({ payload: 'removeLoading' }, '*');
  });
