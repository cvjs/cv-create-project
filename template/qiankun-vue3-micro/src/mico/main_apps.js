/**
 * 3. 配置子应用列表
 */
const apps = [
  {
    name: 'planResource',
    entry: '//localhost:8083',
    container: '#iframe',
    activeRule: '/plan'
  },
  {
    name: 'configration',
    entry: '//localhost:8081',
    container: '#iframe',
    activeRule: '/configure'
  }
];

export default apps;
