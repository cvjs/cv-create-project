import { createApp } from 'vue';
import App from './App.vue';
// 引入vue-router
import router from './router/index';
import './index.css';

const app = createApp(App);
// 挂载到app上
app.use(router);
app.mount('#app');

import './public-path';

let router = null;
let instance = null;

/**
 * @name 单独环境直接实例化vue
 */
const __qiankun__ = window.__POWERED_BY_QIANKUN__;

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './App.vue';
import store from './store';
import selfRoutes from './router/routes';

/**
 * @name 导入自定义路由匹配方法
 */
import routeMatch from './router/routes-match';
/**
 * @name 子应用实例化函数
 * @param {Object} props param0 qiankun将用户添加信息和自带信息整合，通过props传给子应用
 * @description {Array} routes 主应用请求获取注册表后，从服务端拿到路由数据
 * @description {String} 子应用路由前缀 主应用请求获取注册表后，从服务端拿到路由数据
 */
function renderV3(props = {}) {
  const { routes, routerBase, container } = props;
  // Vue.config.productionTip = false;
  router = createRouter({
    history: createWebHistory(__qiankun__ ? routerBase : '/'),
    routes: __qiankun__ ? routeMatch(routes, routerBase) : selfRoutes
  });

  instance = createApp(App)
    .use(router)
    .use(store)
    .mount(container ? container.querySelector('#app') : '#app');
}
