// 引入路由
import router from '@/router/index.js';

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

// import cvIcons from '@/components/cv-icons.vue';

export function bindPlugins(app) {
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
  }
  app.use(ElementPlus, {
    size: 'default'
  });
  // app.component('cvIcons', cvIcons);
  // app.use(cvIcons);

  app.config.globalProperties.jumpsAuth = (path) => {
    // console.log('---use_router', router);
    router.push({
      path: path
    });
  };
}
