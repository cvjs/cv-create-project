import { createApp } from 'vue';
// 引入路由
import router from './router/index.js';
// 引入pinia
import { createPinia } from 'pinia';
const pinia = createPinia();
// 引入插件
import { bindPlugins } from './plugins/index.js';

import App from './App.vue';

import './style.css';

import './demos/ipc';
// If you want use Node.js, the`nodeIntegration` needs to be enabled in the Main process.
// import './demos/node'

const app = createApp(App);

app.use(router);
app.use(pinia);
bindPlugins(app);

app.mount('#app').$nextTick(() => {
  postMessage({ payload: 'removeLoading' }, '*');
});
