import { createRouter, createWebHashHistory } from 'vue-router';
// 开启历史模式
// vue2中使用的mode：history 实现
const routerHistory = createWebHashHistory();
const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      component: () => import('../views/home.vue')
    },
    {
      path: '/demo1',
      component: () => import('../views/demo1/index.vue')
    },
    {
      path: '/demo2',
      component: () => import('../views/demo2/index.vue')
    },

    /**
     * socket调试助手
     */
    {
      path: '/socket/demo1',
      component: () => import('../views/socket/demo1.vue')
    },
    {
      path: '/socket/demo2',
      component: () => import('../views/socket/demo2.vue')
    },
    {
      path: '/socket/demo3',
      component: () => import('../views/socket/demo3.vue')
    }
  ]
});

// ipcRenderer.on('href', (event, arg) => {
//   if (arg) {
//       router.push({ name: arg });
//   }
// });
export default router;
