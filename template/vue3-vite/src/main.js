import { createApp } from 'vue';
import App from './App.vue';

import './assets/css/style.css';

// 引入路由
import router from './router';
import pinia from './store';

const app = createApp(App);
app.use(pinia);
app.use(router);
app.mount('#app');
