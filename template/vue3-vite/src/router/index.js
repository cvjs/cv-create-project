import { createRouter, createWebHistory } from 'vue-router';
// 开启历史模式
const routerHistory = createWebHistory();
const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      component: () => import('../views/home.vue')
    },
    {
      path: '/demo1',
      component: () => import('../views/demo1/index.vue')
    },
    {
      path: '/demo2',
      component: () => import('../views/demo2/index.vue')
    }
  ]
});
export default router;
