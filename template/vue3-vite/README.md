# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).



# vuejs-template

## 简介

基础项目模版

## 安装依赖
```
npm install
```

### 开发环境
```
npm run serve
```

### 开发环境打包
```
npm run build
```

### 验证环境
```
npm run uat
```

### 生产环境
```
npm run production
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
