import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // 打包
  build: {
    sourcemap: false,
    // 自定义底层的 Rollup 打包配置
    rollupOptions: {}
  },
  esbuild: {
    drop: ['console', 'debugger']
  }
});
