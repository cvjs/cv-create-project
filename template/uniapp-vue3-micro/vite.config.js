import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';
// https://vitejs.dev/config/
// https://cvjs.cn
export default defineConfig({
  plugins: [uni()],
  build: {
    cssCodeSplit: false,
    minify: 'terser',
    terserOptions: {
      // 生产环境时移除console
      compress: {
        pure_funcs: ['console.log', 'console.info'],
        drop_console: true,
        drop_debugger: true
      },
      drop_console: true,
      drop_debug: true
    }
  }
});
