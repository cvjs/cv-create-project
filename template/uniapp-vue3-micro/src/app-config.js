// 本地测试接口 ,线上接口
const isOnline = process.env.NODE_ENV === 'development' ? false : true;
// process.env.NODE_ENV === 'production',
const cConfig = {
  _APP_APPNAME_: process.env.VUE_APP_SY_APP_NAME || '应用',
  _APP_VERSION_: process.env.VUE_APP_SY_APP_VERSION || '1.0.0 Beta',
  _APP_PACKAGE_: process.env.VUE_APP_SY_APP_PACKAGE || 'net.xxx.app',

  _APP_PRIVACY_POLICY_: 'http://zhsh.net/privacy_policy_business.html',
  _APP_THEMES_COLOR_: '#409EFF', // #ffa360

  _APP_APPTYPE_: 'organization',

  // 配置页面
  _PAGE_MY_MAIN_: '/pages/yw_main/my_index',
  _PAEG_HOME_MAIN_: '/pages/yw_main/index/index',
  // #ifndef MP
  _PAEG_LOGIN_: '/pages/base_passport/passport_login',
  // #endif
  // #ifdef MP
  _PAEG_LOGIN_: '/pages/base_passport/passport_login_xcx',
  // #endif

  main_startup_img: ['/static/startup/1.png', '/static/startup/2.png', '/static/startup/3.png'],

  // 是否开启注册
  _LOGIN_REG_OPEN_: false,
  _PAY_TYPE_LISTS: {
    pay_type_weixin: true, //微信支付
    pay_type_alipay: true, //支付宝支付
    pay_type_integral: false, //积分支付
    pay_type_balance: true, //余额支付
    pay_type_profit: true, //佣金支付
    pay_type_zhuanzhang: false, //线下转账
    pay_type_unionpay: false, //银联支付
    pay_type_yunshanfu: false, //云闪付
    pay_type_jifen: false //积分支付
  }
};

// 定义出口
export default cConfig;
