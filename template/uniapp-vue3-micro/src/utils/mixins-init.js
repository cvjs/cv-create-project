import pagesDiyPlus from '@/pages-diy-plus.js';
export default {
  data() {
    return {
      isJumpsAuth: this.$wvSub ? true : false
    };
  },
  onLoad() {},
  onShow() {
    if (!this.isJumpsAuth) {
      this._initAuthenticate();
    }
  },
  methods: {
    _initAuthenticate() {
      let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
      let curRoute = routes[routes.length - 1].route; // 获取当前
      curRoute = curRoute.replace(/\//g, '_');
      let authData = pagesDiyPlus[curRoute];
      if (!authData || authData.authLogin === false) {
        /* 无需验证的哟 */
        return;
      }
      if (!this.syAppAccess()) {
        uni.showModal({
          title: '提示',
          content: '您还未登录，请先登录',
          confirmText: '去登录',
          success: (modalRes) => {
            if (modalRes.confirm) {
              this.$jp.jumpsPage('base_passport/passport_login');
            } else {
              if (routes.length > 1) {
                /* 非tab页面返回一级 */
                uni.navigateBack({
                  delta: 1
                });
              } else {
                /* tab页面回到首页 */
                uni.switchTab({
                  url: '/'
                });
              }
            }
          }
        });
      }
    }
  }
};
