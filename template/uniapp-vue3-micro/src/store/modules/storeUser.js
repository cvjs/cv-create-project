import Vue from 'vue';

// 小程序自动登录
// ******* 自动登录***************************************************************
function authLogin_xcx(skip, callback) {
  uni.login({
    success: (res) => {
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      Vue.prototype.$request
        .flagPost('V2_PASSPORT_PASSPORT', {
          handleType: 'WxxcxAuthLogin',
          code: res.code
        })
        .then((successRes) => {
          // 登录成功 保存信息至js和缓存
          Vue.prototype.$store.dispatch('storeUser/ssSignIn', successRes.data || {});
          typeof callback == 'function' && callback(successRes);
        })
        .catch((resData) => {
          uni.hideToast();
          uni.showModal({
            title: '提示',
            content: '登录出错，请重试',
            showCancel: false
          });
          switch (apiResStatus) {
            case '10018':
              // 返回登录失败1，改用户未注册过此小程序
              // 是否跳过注册
              if (skip) {
                console.log('11111111111跳过登录');
                return;
              }
              uni.hideToast();
              console.log('未登陆过微信(用户未注册)');
              if (getCurrentPages().length > 1) {
                uni.redirectTo({
                  url: '/pages/base_passport/passport_login_xcx_index'
                });
              } else {
                uni.navigateTo({
                  url: '/pages/base_passport/passport_login_xcx_index'
                });
              }
              break;
            case '404':
              // 返回登录失败1，改用户未注册过此小程序
              console.log('未登陆过微信(用户未授权)');
              break;
          }
        });
    }
  });
  // #ifdef H5
  uni.showModal({
    title: '提示',
    content: '[10003]用户错误，请重新登录',
    showCancel: false,
    success: (res) => {
      if (res.confirm) {
        // #ifndef MP
        uni.navigateTo({
          url: '/pages/base_passport/passport_login'
        });
        // #endif
        // #ifdef MP
        uni.navigateTo({
          url: '/pages/base_passport/passport_login_xcx'
        });
        // #endif
      }
    }
  });
  // #endif
}
/**
 * 利用缓存的方式 如果用户登录成功后，将信息缓存下来
 * @createTime 2019-04-05
 */
let cacgeAuthData = uni.getStorageSync('syCacheAppAccess');

const state = {
  loginProvider: '',
  openid: cacgeAuthData['token'] ? cacgeAuthData['token'] : null, // openid
  has_login: false,
  syOpenAppAccess: cacgeAuthData, // 获取用户信息
  /**
   *
   * 环信im需要用到的
   *
   */
  userInfo: null,
  saveFriendList: [],
  saveGroupInvitedList: [],
  isIPX: false, // 是否为iphone X
  easeIm_CurOpenOpt: {},
  easeIm_closed: {},
  userGeoLocation: '----' //用户实时位置
};

try {
  let tokenAccess = uni.getStorageSync('syCacheAppToken') || null;
  if (tokenAccess) {
    state.has_login = true;
  } else {
    state.has_login = false;
    // uni.setStorageSync('syCacheAppToken', '');
    // #ifdef MP
    authLogin_xcx(true);
    // #endif
    // return false;
  }
} catch (e) {
  // error
}
const getters = {
  userGeoLocation: (state) => state.storeUser.userGeoLocation,
  getHasLogin: (state) => state.has_login,
  syOpenAppAccess: (state) => state.syOpenAppAccess //用户的身份
};
const mutations = {
  // 设置父应用信息
  SS_GEO_LOCATION(state, data) {
    state.userGeoLocation = data;
  },
  // 登录
  SS_SIGNIN(state, data) {
    state.loginProvider = data.provider || '';
    state.openid = data.token;
    state.userinfo = data;
    state.syOpenAppAccess = data;
    // 缓存用户信息
    state.has_login = true;

    uni.setStorageSync('syCacheAppToken', data.token_access || '');
    uni.setStorageSync('syCacheAppJwt', data.token_jwt || '');
    uni.setStorageSync('syCacheAppAccess', data || {});
    uni.setStorage('syCacheAppToken', data.token_access || '');

    Vue.prototype.$request.setHeaders({
      syOpenAppToken: data.token_access || '',
      Authorization: 'Bearer ' + data.token_jwt || ''
    });
  },
  // 退出登录
  SS_LOGOUT(state) {
    state.has_login = false;
    state.openid = null;
    state.userinfo = [];

    let tokenAccess = uni.getStorageSync('syCacheAppToken') || null;
    // 清理用户信息
    uni.setStorageSync('syCacheAppToken', '');
    uni.setStorageSync('syCacheAppAccess', '');
    uni.setStorageSync('syCacheAppJwt', '');

    uni.removeStorage('syCacheAppToken');
    uni.removeStorage('syCacheAppAccess');
    uni.removeStorage('syCacheAppJwt');

    uni.removeStorageSync('syCacheAppToken');
    uni.removeStorageSync('syCacheAppAccess');
    uni.removeStorageSync('syCacheAppJwt');

    Vue.prototype.$request.setHeaders({
      syOpenAppToken: '',
      Authorization: ''
    });
    // uni.removeStorageSync('expire_time');
    // uni.setStorageSync('is_vip', 0);

    // #ifdef MP
    authLogin_xcx(false);
    // #endif

    // uni.clearStorage();
    // uni.clearStorageSync();
  },
  SS_SOpenid(state, openid) {
    state.openid = openid;
  },
  save(state, data) {
    var user = state.userinfo;
    for (let i in data) {
      user[i] = data[i];
      if (i == 'openid') {
        state.openid = data[i];
      }
    }
    state.userinfo = user;
  },
  getUseriInfo(state) {}
};

const actions = {
  // 登录
  ssSignIn({ commit }, payload) {
    commit('SS_SIGNIN', payload);
  },
  // 退出登录
  ssLogout({ commit }, payload) {
    commit('SS_LOGOUT', payload);
  },
  ssInfo({ commit }, payload) {
    return '';
  },
  setUserGeoLocation({ commit }, data) {
    commit('SS_GEO_LOCATION', data);
  },
  // lazy loading openid
  async getUserOpenId({ commit }, state) {
    return await new Promise((resolve, reject) => {
      if (state.openid) {
        resolve(state.openid);
      } else {
        uni.login({
          success: (data) => {
            commit('SS_SIGNIN');
            setTimeout(function () {
              //模拟异步请求服务器获取 openid
              const openid = '123456789';
              console.log('uni.request mock openid[' + openid + ']');
              commit('SS_SOpenid', openid);
              resolve(openid);
            }, 1000);
          },
          fail: (err) => {
            console.log('uni.login 接口调用失败，将无法正常使用开放接口等服务', err);
            reject(err);
          }
        });
      }
    });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
