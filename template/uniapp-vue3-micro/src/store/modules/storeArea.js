const state = {
  syCacheGeoCity: uni.getStorageSync('syCacheGeoCity'),
  locationInfo: uni.getStorageSync('locationInfo')
};
const getters = {
  syCacheGeoCity: (state) => state.syCacheGeoCity, //当前所在城市
  locationInfo: (state) => state.locationInfo //当前详细位置
};
const mutations = {
  SET_AREA_INFO: (state, syCacheGeoCity) => {
    state.syCacheGeoCity = syCacheGeoCity;
    uni.setStorageSync('syCacheGeoCity', syCacheGeoCity);
  },
  SET_LOCATION_INFO: (state, locationInfo) => {
    state.locationInfo = locationInfo;
    uni.setStorageSync('locationInfo', locationInfo);
  }
};
const actions = {
  setAreaInfo({ commit, state }, payload) {
    commit('SET_AREA_INFO', payload);
  },
  setLocationInfo({ commit, state }, payload) {
    commit('SET_LOCATION_INFO', payload);
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
