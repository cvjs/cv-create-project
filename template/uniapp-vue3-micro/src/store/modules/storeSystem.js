import cConfig from '@/app-config.js';
function cache_system_info() {
  uni.getSystemInfo({
    success: function (e) {
      let customBar = 0;
      let statusBar = 0;
      // #ifndef MP
      statusBar = e.statusBarHeight;
      if (e.platform == 'android') {
        customBar = e.statusBarHeight + 49;
      } else {
        customBar = e.statusBarHeight + 44;
      }
      // #endif

      // #ifdef MP-WEIXIN || MP-QQ
      statusBar = e.statusBarHeight;
      let capsule = wx.getMenuButtonBoundingClientRect();
      if (capsule) {
        customBar = capsule.bottom + capsule.top - e.statusBarHeight;
      } else {
        customBar = e.statusBarHeight + 49;
      }
      // #endif
      // #ifdef MP-ALIPAY
      statusBar = e.statusBarHeight;
      customBar = e.statusBarHeight + e.titleBarHeight;
      // #endif
      e['statusBar'] = statusBar;
      e['customBar'] = customBar;
      e['clientWidth'] = e.clientWidth;
      e['clientHeight'] = e.windowHeight;
      e['rpxR'] = 750 / e['clientWidth']; // 比例
      e['clientCalc'] = e['clientHeight'] * e['rpxR'];
      // this.pxr = 1 / 	e['rpxR'] ;

      uni.setStorageSync('syCacheSystemInfo', e);
    }
  });
}
cache_system_info();

const state = {
  cacheSystem: uni.getStorageSync('syCacheSystemInfo'),
  systemInfo: uni.getStorageSync('syCacheSystemInfo'),
  //设置用户主题
  themesData: {
    color: cConfig._APP_THEMES_COLOR_,
    /* 0 - 25 , 默认5 */
    font: 5,
    bgColor: '#ff5400',
    bgTextColor: '#ffffff'
  }
};
const getters = {
  cacheSystem: (state) => state.system.cacheSystem, //系统信息
  renderBgColor: function (state) {
    return state.bgColor;
  },
  renderBgTextColor: function (state) {
    return state.bgTextColor;
  }
};
const mutations = {
  SETsyCacheSystemInfo: (state, cacheSystem) => {
    state.cacheSystem = cacheSystem;
    uni.setStorageSync('syCacheSystemInfo', cacheSystem);
  },
  setThemesData: (state, data) => {
    state.themesData = data;
    uni.setStorageSync('themesData', data);
  },
  setBgColor: function (state, usertype) {
    state.bgColor = usertype;
  },
  setBgTextColor: function (state, usertype) {
    state.bgTextColor = usertype;
  }
};
const actions = {
  setThemesData({ commit, state }, payload) {
    commit('setThemesData', payload);
  },
  cacheSystemInfo({ commit, state }, payload) {
    commit('SETsyCacheSystemInfo', payload);
  },
  setUserBgColor: function (context, usertype) {
    context.commit('setBgColor', usertype);
  },
  setUserBgTextColor: function (context, usertype) {
    context.commit('setBgTextColor', usertype);
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
