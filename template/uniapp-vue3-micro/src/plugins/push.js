import Vue from 'vue';
/**
 * 推送类
 * @version 2021-11-11
 */
var pushClass = {
  /**
   * 推送绑定
   */
  pushBind() {
    // #ifdef APP-PLUS
    const clientInfo = plus.push.getClientInfo();
    let pushUser = {
      cid: clientInfo.clientid, //客户端标识
      clientid: clientInfo.clientid,
      appid: clientInfo.appid,
      appkey: clientInfo.appkey,
      userName: '用户名',
      userRole: '用户角色'
    };
    Vue.prototype.$request
      .flagPost('', {
        ...pushUser
      })
      .then((successRes) => {
        console.log('*************绑定推送**************');
        console.log(JSON.stringify(successRes));
      });
    // #endif
  },
  /**
   * 推送监听
   */
  pushLinstener() {
    //#ifdef APP-PLUS

    //开启推送
    // uni.subscribePush({
    //   provider: "unipush",
    //   success: function (res) {
    //     console.log("success:" + JSON.stringify(res));
    //   }
    // });
    // let osname = plus.os.name
    // console.log(osname)
    // if (osname == 'Android') {
    //   uni.onPush({
    //     provider: "unipush",
    //     success: function () {
    //       console.log("监听透传成功");
    //     },
    //     callback: function (data) {
    //       console.log("接收到透传数据：" + JSON.stringify(data));
    //       plus.push.createMessage(data.data, {});
    //     }
    //   });
    // }
    // // 监听在线消息事件
    // if (osname == 'iOS') {
    //   plus.push.addEventListener('receive', function (msg) {

    // msg.content =  '欢迎使用Html5 Plus创建本地消息！';
    //     plus.push.createMessage(msg.content, "LocalMSG", {
    //       cover: false
    //     });
    //     setTimeout(() => {
    //       plus.push.clear();
    //     }, 3000)
    //   }, false);
    // }

    var info = plus.push.getClientInfo();
    console.log(JSON.stringify(info));

    /**
     *  使用5+App的方式进行监听消息推送
     *
     * 5+  push 消息推送 ps:使用:H5+的方式监听，实现推送
     */
    plus.push.addEventListener(
      'click',
      function (msg) {
        //这里写业务逻辑
        console.log('click:' + JSON.stringify(msg));
        console.log(msg.payload);
        console.log(JSON.stringify(msg));
        // onLaunch 生命周期里，页面跳转有问题,跳不过去
        // 应该是页面还没加载，加上定时后，就可以了；
        // setTimeout(() => {
        //   uni.navigateTo({
        //     url: `pages/charging/chargeCoupon?data=${JSON.parse(msg.payload)}`
        //   })
        // }, 1000)

        // plus.ui.alert("click:"+ msg);
        // plus.nativeUI.toast('push click' + msg );
        // let payload = JSON.parse(msg.payload);
        // let msgType = payload.type || '';
        // if (msgType == 'open_news_detail') {
        //   let article_id = payload.article_id;
        //   setTimeout(function () {
        //     if (article_id) {
        //       this.jumpsPage('/yw_sns_article/article_detail?article_id=' + article_id);
        //     } else {
        //       this.jumpsPage('/yw_sns_pages/msg_index');
        //     }
        //   }, 1000);
        // }
      },
      false
    );

    /**
     * 监听在线消息事件
     */
    plus.push.addEventListener(
      'receive',
      function (msg) {
        //业务代码
        // plus.ui.alert(2);
        // plus.ui.alert("receive:"+ message);
        // plus.nativeUI.toast('push receive' + message );
        //这里可以写跳转业务代码

        let recevice = JSON.stringify(msg);
        console.log('recevice:' + recevice);

        let payload = JSON.parse(msg.payload);
        let msgType = payload.type || '';

        // if (msgType == 'open_news_detail') {
        //   let article_id = payload.article_id;
        //   setTimeout(() => {
        //     if (article_id) {
        //       this.jumpsPage('/yw_sns_article/article_detail?article_id=' + article_id);
        //     } else {
        //       this.jumpsPage('/yw_sns_pages/msg_index');
        //     }
        //   }, 1000);
        // }

        // console.log(msg)
        // plus.push.createMessage(msg.content, msg.payload, { cover: false });
      },
      false
    );

    //#endif
  }
};

Vue.prototype.$push = pushClass;
