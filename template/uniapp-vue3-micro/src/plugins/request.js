import Vue from 'vue';
import $store from '@/store';

import RequestClass from '@10yun/cv-mobile-ui/plugins/request.js';

let reqPubData = {};
// #ifdef MP-WEIXIN
const bid = process.env.VUE_APP_SY_AUTH_BID || 0;
if (bid != undefined && bid > 0) {
  reqPubData.business_id = bid;
}
// #endif

import ApiFlagAll from '@/api/index.js';

let systemInfo = uni.getSystemInfoSync();

/**
 * 【初始化绑定】 request
 */
var requestObj = new RequestClass({
  baseURL: process.env.VUE_APP_SY_API_URL || '',
  flagMap: ApiFlagAll, //接口配置参数
  storeHandle: $store, //store句柄
  needMethods: ['PATCH'],
  headers: {
    syOpenAppRole: process.env.VUE_APP_SY_APP_ROLE || '',
    syOpenAppProject: process.env.VUE_APP_SY_APP_PRO || '',
    syOpenAppId: process.env.VUE_APP_SY_APP_ID || '',
    syOpenAppToken: uni.getStorageSync('syCacheAppToken'),
    syOpenClientPlatform: systemInfo.platform || '',
    Authorization: 'Bearer ' + uni.getStorageSync('syCacheAppJwt')
  },
  requests: reqPubData || {}
});
Vue.prototype.$request = requestObj;
