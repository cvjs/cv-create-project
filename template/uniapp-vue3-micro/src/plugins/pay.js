import Vue from 'vue';

/**
 * 前往支付 - 收银台
 *
 * @cashier_paytype // type 传入 1 正常情况（余额、微信） // 2 充值 // 3 大额（余额、微信、银行线下转账）
 * @back_delta 点击返回层级 必须大于1
 * @to_url 点击按钮跳转url
 * @title 成功页标题
 * @msg 成功页内容
 * @button 跳转按钮标题
 * @type 跳转类型 1 redirectTo 0(default) navigateTo
 */
//  旧的：function(cashier_paytype, finance_id, back_delta, to_url, title, msg, button, type)
Vue.prototype.jumpsCommonCashier = function (cashierOpt) {
  let cashierDef = {
    finance_id: 0, //
    cashier_paytype: '', // type 传入 1 正常情况（余额、微信） // 2 充值 // 3 大额（余额、微信、银行线下转账）
    payok_back_delta: 0, // 点击返回层级 必须大于1
    payok_btnurl: '', // 跳转按钮,跳转url
    payok_title: '支付成功', // 成功页标题
    payok_msg: '支付成功，请前往我的订单查看', // 成功页内容
    payok_btnname: '查看详情', // 跳转按钮,标题
    payok_btntype: 0 // 跳转按钮,跳转类型 1 redirectTo 0(default) navigateTo
  };
  let paydata = Object.assign(cashierDef, cashierOpt);
  if (!paydata.cashier_paytype) {
    paydata.cashier_paytype = this.$cConfig._PAY_TYPE_LISTS;
  } else {
    for (const key in paydata.cashier_paytype) {
      if (paydata.cashier_paytype[key]) {
        if (this.$cConfig._PAY_TYPE_LISTS[key] == false) {
          paydata.cashier_paytype[key] = false;
        }
      }
    }
  }
  let finance_id = paydata.finance_id || '';
  if (finance_id == '') {
    uni.showToast({
      title: '支付单号错误～',
      duration: 2000
    });
    return false;
  }
  let paydata2 = encodeURIComponent(JSON.stringify(paydata));
  uni.setStorageSync('syCachePayData', paydata2);
  if (paydata.payok_btntype == 1) {
    uni.redirectTo({ url: '/pages/common_pay/pay_paying' });
  } else {
    uni.navigateTo({ url: '/pages/common_pay/pay_paying' });
  }
};
