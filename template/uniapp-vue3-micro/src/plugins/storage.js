import StorageClass from '@10yun/cv-mobile-ui/plugins/storage.js';

/**
 * 实例，缓存数据
 */
var StorageObj = new StorageClass({
  prefix: '__sy__',
  suffix: ''
});
export default StorageObj;
