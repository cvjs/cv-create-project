import Vue from 'vue';
import $store from '@/store';

import cConfig from '@/app-config.js';

import JumpsClass from '@10yun/cv-mobile-ui/plugins/jumps.js';

/**
 * 实例，页面跳转
 */
let jumpsObj = new JumpsClass({
  page_my: cConfig._PAGE_MY_MAIN_,
  page_home: cConfig._PAEG_HOME_MAIN_,
  page_login: cConfig._PAEG_LOGIN_,
  storeHandle: $store //store句柄
});

Vue.prototype.$jp = jumpsObj;
