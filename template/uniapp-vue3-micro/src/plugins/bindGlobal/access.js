import Vue from 'vue';

/**
 * 登录相关
 */
Vue.prototype.syAppAccess = function (key) {
  // 获取当前时间戳
  let currTime = parseInt(new Date().getTime() / 1000);

  key = key || '';
  try {
    let syAppAccess = uni.getStorageSync('syCacheAppAccess') || null;
    if (!syAppAccess) {
      this.$store.dispatch('storeUser/ssLogout');
      return false;
    }
    // 超出时间
    if (currTime > syAppAccess.token_time) {
      this.$store.dispatch('storeUser/ssLogout');
      return false;
    }
    if (key != '') {
      switch (key) {
        case 'ucrules_arr':
          return syAppAccess[key] || [];
          break;
        default:
          return syAppAccess[key] || null;
      }
      return syAppAccess[key] || null;
    }
    return syAppAccess;
  } catch (e) {
    console.log('syAppAccess()取缓存错误');
  }
};
