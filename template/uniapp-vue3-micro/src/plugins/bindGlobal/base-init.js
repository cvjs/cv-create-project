import Vue from 'vue';

// 扫码
Vue.prototype.$scanCode = function () {
  // #ifdef H5
  uni.showToast({
    title: '请使用APP扫码',
    duration: 2000,
    icon: 'none'
  });
  // #endif
  // #ifndef H5
  // 允许从相机和相册扫码
  uni.scanCode({
    success: (res) => {
      console.log(res);
      console.log('条码/二维码类型：' + res.scanType);
      console.log('条码/二维码内容：' + res.result);
      analyseUrl(res.result);
    }
  });
  // #endif
  /* 解析url */
  function analyseUrl(url) {
    uni.navigateTo({
      url: url.split('#')[1]
    });
  }
};
