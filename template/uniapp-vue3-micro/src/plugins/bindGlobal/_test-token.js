if (process.env.NODE_ENV === 'development' && process.env.VUE_APP_SY_TEST_TOKEN) {
  uni.setStorageSync('syCacheAppToken', `${process.env.VUE_APP_SY_TEST_TOKEN}`);
}
