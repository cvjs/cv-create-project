import Vue from 'vue';

/**
 * 读取配置
 */
import cConfig from '@/app-config.js';
Vue.prototype.$cConfig = cConfig;

// 空方法
Vue.prototype.emptyFunc = function () {};
// 图片错误
Vue.prototype.imgError = function () {};

Vue.prototype.setData = function (obj) {
  let keys = [];
  let val, data;
  Object.keys(obj).forEach((key) => {
    keys = key.split('.');
    val = obj[key];
    data = this.$data;
    keys.forEach((key2, index) => {
      if (index + 1 == keys.length) {
        this.$set(data, key2, val);
      } else {
        if (!data[key2]) {
          this.$set(data, key2, {});
        }
      }
    });
  });
};
