import App from './App';
import store from './store';

/**
 * @name 统一注册外部插件、样式、服务等
 */
const pluginsFiles = require.context('./plugins/bindGlobal/', true, /.js$/);
pluginsFiles.keys().reduce((modules, modulePath) => {
  pluginsFiles(modulePath);
}, {});

import Vue from 'vue';
Vue.config.productionTip = false;
Vue.prototype.$store = store;

App.mpType = 'app';
const app = new Vue({
  ...App
});
app.$mount();
