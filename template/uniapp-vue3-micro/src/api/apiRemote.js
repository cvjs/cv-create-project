export async function loadConfigApi(apiStr) {
  apiStr = apiStr || '';
  var url = process.env.VUE_APP_SY_API_URL || '';
  try {
    // 所有缓存
    var cacheAllFlag = uni.getStorageSync('syCacheApiFlag') || {};
    var chaceAllArr = Object.keys(cacheAllFlag);
    // 当前缓存
    var cacheItemFlag = cacheAllFlag[apiStr] || {};
    var cacheItemArr = Object.keys(cacheItemFlag);
    // 缓存时间
    var cacheExpires = cacheAllFlag['expires'] || 0;
    // 当前时间
    let currTime = parseInt(new Date().getTime() / 1000);
    /**
     * 判断
     */
    if (chaceAllArr.length == 0 || cacheItemArr.length == 0 || currTime > cacheExpires) {
      cacheItemFlag = await new Promise((resolve, reject) => {
        uni.request({
          url: url + 'load-config/api_json/' + apiStr,
          data: {},
          header: {
            syOpenAppProject: process.env.VUE_APP_SY_APP_PRO || '',
            syOpenAppId: process.env.VUE_APP_SY_APP_ID || ''
          },
          success: (succRes) => {
            resolve((succRes.data && succRes.data.data) || {});
          },
          fail: (errorRes) => {
            reject({});
          }
        });
      });
      cacheAllFlag[apiStr] = cacheItemFlag;
      cacheAllFlag['expires'] = currTime + 60 * 60 * 24 * 7; // 7天
      uni.setStorageSync('syCacheApiFlag', cacheAllFlag);
      // console.log('22222---', cacheItemFlag);
    }
    return cacheItemFlag;
  } catch (error) {
    return {};
  }
}
