import Vue from 'vue';

export default function (render) {
  if (!window.__POWERED_BY_QIANKUN__) {
    render();
  }
}
// qiankun bootstrap hook
export function bootstrap() {
  console.log('nuxt app bootstraped');
}

// qiankun mount hook
export async function mount(render, props) {
  await render();
}

// call after nuxt rendered
export function mounted(instance, props) {
  if (props.sdk) {
    Vue.prototype.$sdk = props.sdk;
  }
}
// qiankun update hook
export async function update() {}
export function beforeUnmount(instance) {}
export function unmount() {}
