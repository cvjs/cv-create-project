document.addEventListener(
  'plusready',
  function () {
    var _BARCODE = 'plugintest';
    // var plus_bridge = window.plus.bridge || plus.bridge;
    var plus_bridge = window.plus.bridge;
    var plugintest = {
      PluginTestFunction: function (Argus1, Argus2, Argus3, Argus4, successCallback, errorCallback) {
        var success =
            typeof successCallback !== 'function'
              ? null
              : function (args) {
                  successCallback(args);
                },
          fail =
            typeof errorCallback !== 'function'
              ? null
              : function (code) {
                  errorCallback(code);
                };
        callbackID = plus_bridge.callbackId(success, fail);

        return plus_bridge.exec(_BARCODE, 'PluginTestFunction', [callbackID, Argus1, Argus2, Argus3, Argus4]);
      },
      PluginTestFunctionArrayArgu: function (Argus, successCallback, errorCallback) {
        var success =
            typeof successCallback !== 'function'
              ? null
              : function (args) {
                  successCallback(args);
                },
          fail =
            typeof errorCallback !== 'function'
              ? null
              : function (code) {
                  errorCallback(code);
                };
        callbackID = plus_bridge.callbackId(success, fail);
        return plus_bridge.exec(_BARCODE, 'PluginTestFunctionArrayArgu', [callbackID, Argus]);
      },
      PluginTestFunctionSync: function (Argus1, Argus2, Argus3, Argus4) {
        return plus_bridge.execSync(_BARCODE, 'PluginTestFunctionSync', [Argus1, Argus2, Argus3, Argus4]);
      },
      PluginTestFunctionSyncArrayArgu: function (Argus) {
        return plus_bridge.execSync(_BARCODE, 'PluginTestFunctionSyncArrayArgu', [Argus]);
      }
    };
    window.plus.plugintest = plugintest;
  },
  true
);
