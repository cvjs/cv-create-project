import { createSSRApp } from 'vue';

import pinia from '@/store/index.js';
import App from './App.vue';

/**
 * @name 统一注册外部插件、样式、服务等
 */
export function createApp() {
  const app = createSSRApp(App);
  app.use(pinia);
  return {
    app
  };
}
