export default [
  {
    text: '配置',
    collapsed: true,
    items: [
      { text: '配置 Vite', link: '/config/' },
      { text: '共享选项', link: '/config/shared-options' }
    ]
  }
];
