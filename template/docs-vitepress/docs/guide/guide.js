export default [
  {
    text: '指引',
    items: [
      { text: '开始', link: '/guide/' },
      { text: 'vite', link: '/guide/vite' },
      { text: 'vitepress', link: '/guide/vitepress' },
      { text: '为什么选 Vite', link: '/guide/why' }
    ]
  },
  {
    text: '这个菜单带下拉框',
    collapsed: false,
    items: [
      { text: '插件 API', link: '/guide/api-plugin' },
      { text: 'HMR API', link: '/guide/api-hmr' },
      { text: 'JavaScript API', link: '/guide/api-javascript' }
    ]
  }
];
