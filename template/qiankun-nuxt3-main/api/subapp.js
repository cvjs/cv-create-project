export default {
  code: 0,
  payload: [
    {
      name: 'vue-app',
      activeRule: '/vue',
      entry: 'http://localhost:17101/'
    },
    {
      name: 'nuxt-app',
      activeRule: '/nuxt',
      entry: 'http://localhost:17102/'
    },
    {
      name: 'product-app',
      activeRule: '/product',
      entry: 'http://localhost:17103/'
    }
  ],
  message: 'success'
};
