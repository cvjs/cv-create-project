# create-cvjs

自定义脚手架模版

搭建你的第一个cvjs项目

> **兼容性 Note:**
> Vite需要[Node.js](https://nodejs.org/en/)版本 20+。然而，有些模板需要更高的Node.js版本才能工作，如果您的包管理器对此发出警告，请升级。  

### 参考

参考 https://github.com/vitejs/create-vite-app

https://github.com/vitejs/vite/tree/main/packages/create-vite


### 使用方式 

```bash
# npm 方式
npm create cvjs

# pnpm 方式
pnpm create cvjs

# yarn 方式
yarn create cvjs

```

然后按照提示操作！

也可以通过其他命令行选项直接指定项目名称和要使用的模板。
例如，要为uniapp项目搭建脚手架，请运行：

```bash
# npm 6.x
npm create cvjs my-uniapp1 --template mobile

# npm 7+, extra double-dash is needed:
npm create cvjs my-uniapp1 -- --template mobile

# yarn
yarn create vite my-uniapp1 --template mobile

# pnpm
pnpm create vite my-uniapp1 --template vue
```


### 预设模版

当前支持的模板预设包括：

js => javascript  
ts => typescript  


| 模版                | node |   js    |   ts    |  vite   | 说明             |
| :------------------ | :--- | :-----: | :-----: | :-----: | :--------------- |
| vue3-vite           | 20+  | &check; | &check; | &check; | vue3应用         |
| vue3-vite-electron  | 20+  | &check; | &check; | &check; | electron 应用    |
| qiankun-nuxt3-micro | 20+  | &check; |         | &check; | qiankun子应用SSR |
| qiankun-vue3-micro  | 20+  | &check; |         | &check; | qiankun 子应用   |
| qiankun-vue2-main   |      | &check; |         |         | qiankun 主应用   |
| qiankun-vue2-micro  |      | &check; |         |         | qiankun子应用    |
| ssr-nuxt3           |      | &check; |         | &check; | vue3网站SSR      |
| docs-vitepress      |      | &check; |         | &check; | vue3文档系统     |
| uniapp-vue3-main    | 20+  | &check; |         | &check; | uniapp主应用     |
| uniapp-vue3-micro   | 20+  | &check; |         | &check; | uniapp子应用     |
| npm-libs            |      | &check; |         | &check; | 工具库           |
| npm-ui              |      | &check; |         |         | UI库             |

### 目录结构 

~~~

├── src                     # 程序
      ├── index.ts

├── template                # 模版
      ├── javascript         # js 版本
          ├── vue3-vite
          ├── vue3-vite-electron
          ├── uniapp-app        # 移动端主应用
          ├── uniapp-micro      # 移动端子应用

      ├── typescript         # ts 版本


~~~